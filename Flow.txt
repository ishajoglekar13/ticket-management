FLOW

There are mainly two categories of a user : 
1. Team Leader
2. Team Member

On Logging in the user will be redirected to the dashboard, the dashboard will have different options depending on the role of the User.

Functionality Provided To A Leader:
a) View Users
	1. Leader can view personal and work details of a member
		1.1 Leader can view advanced details(login,logout,tasks performance of the particular session) of a member if the member belongs to his/her team
	2. Leader can add a member to his/her team if the member isn't currently assigned to any team.
	3. Leader can remove a member from his/her team if the member doesn't have any active tasks.
b) View Team
	1. Leader can view his/her team member's Active,Completed and Failed Tasks
	2. Leader can assign a task to a member
	3. Leader can remove a member from his/her team if the member doesn't have any active tasks.
c) Tasks
	1. Assign Task
		1.1 Leader can assign a task automatically/manually to his/her team members
	2. Manage Tasks
		2.1 Leader can re-assign an unresolved task to another member
		2.2 Leader can edit due date of an existing task
	3. Review Tasks
		3.1 Leader can review completed tasks, mark them as resolved or reject them
			3.1.1 The rejected tasks can be re-assigned to other member

Functionality Provided To A Member:
a)View Team
	1. Member can view his/her team member's Active,Completed and Failed Tasks
b)Tasks
	1.Member can view his/her tasks (active,completed,failed)
	2.Member can complete task or give up


Other Functionalities :
1. Notifications :
On the necessary actions, leaders and members are notified, for instance
A member will be notified when he/she is assigned a task, added/removed to/from a team
A leader will be notified when his/her team member complete or give up on a task

2.Dashboard
Best Member, Best Leader and Top 3 Members will be displayed as per their efficiency
If a member is logged in : his/her task count will be displayed

	
		
	
