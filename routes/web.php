<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home','HomeController@index')->name('dashboard.index')->middleware('auth');

Auth::routes();
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');








Route::middleware(['auth'])->group(function(){
Route::get('/dashboard','HomeController@index')->name('dashboard.index');

Route::resource('teams','TeamsController');


Route::resource('users','UsersController')->middleware(['leader']);
Route::post('users/details/{user}','UsersController@details')->name('users.details')->middleware(['leader']);
Route::put('/users/{user}/make-team-member','UsersController@makeTeamMember')->name('users.make-team-member')->middleware(['leader']);
Route::put('/users/{user}/remove-team-member','UsersController@removeTeamMember')->name('users.remove-team-member')->middleware(['leader']);
// Route::resource('teams','TeamsController');

Route::resource('tasks','TasksController');
// Route::get('/mytasks','TasksController@mytasks')->name('tasks.mytasks');
Route::get('/review','TasksController@review')->name('tasks.review')->middleware(['leader']);
// Route::get('/completed/{task}','TasksController@completed')->name('tasks.completed');
Route::get('/resolved/{task}','TasksController@resolved')->name('tasks.resolved')->middleware(['leader']);
Route::get('/unresolved/{task}','TasksController@unresolved')->name('tasks.unresolved')->middleware(['leader']);
Route::post('/tasks/{task}','TasksController@update')->name('tasks.update')->middleware(['leader']);
Route::post('/tasks/assigned/{user}','TasksController@assign')->name('tasks.assign')->middleware(['leader']);
Route::get('/mytasks','TasksController@mytasks')->name('tasks.mytasks');
Route::get('/completed/{task}','TasksController@completed')->name('tasks.completed');
Route::get('/reassign/{task}','TasksController@reassign')->name('tasks.reassign');
Route::get('/poll','TasksController@poll')->name('tasks.poll');
Route::get('/giveup/{task}','TasksController@giveup')->name('tasks.giveup');
Route::get('/claim-task/{task}','TasksController@claim')->name('tasks.claim');
});


?>