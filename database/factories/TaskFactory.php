<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Task;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$factory->define(Task::class, function (Faker $faker) {
    $statusArray = ['assigned','unresolved'];
    return [
        'name'=> $faker->sentence(rand(3,7)),
        'status'=>Arr::random($statusArray, 1)[0],
        'level'=>rand(0,2),
        'user_id'=>\App\User::pluck('id')->random(),
        'assigned_on'=>Carbon::now(),
        'due_on'=>Carbon::now()->addHour(24)
        
    ];
});




