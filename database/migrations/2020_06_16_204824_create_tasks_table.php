<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('status');
            $table->string('level');
            $table->unsignedBigInteger('user_id')->nullable();
         
            $table->timestamp('assigned_on')->nullable();
            $table->timestamp('completed_on')->nullable();
            $table->timestamp('due_on')->nullable();

            $table->timestamps();

            $table->foreign('user_id')
            ->references('id') 
            ->on('users');
        

        
   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
