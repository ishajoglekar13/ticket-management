<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\Hash;
use App\Task;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        \App\User::create([
            'name'=>'Muskaan Aswani',
            'email'=>'muskan@gmail.com',
            'password'=>Hash::make('12345678'),
            'role'=>'leader',
            'address'=>'Ulhasnagar, Mumbai',           
            'efficiency'=>rand(10,50),
            'designation_id'=>\App\Designation::pluck('id')->random(),
            'team_id'=>1,
            'dob'=>date('Y-m-d',(strtotime("2001/02/10")))
            
        ]);
        \App\User::create([
            'name'=>'Isha Joglekar',
            'email'=>'isha@gmail.com',
            'password'=>Hash::make('12345678'),
            'role'=>'leader',
            'address'=>'Bandra, Mumbai',
            'efficiency'=>rand(10,50),
            'designation_id'=>\App\Designation::pluck('id')->random(),
            'team_id'=>2,
            'dob'=>date('Y-m-d',(strtotime("2001/04/13")))
        ]);
        \App\User::create([
            'name'=>'John Doe',
            'email'=>'john@gmail.com',
            'password'=>Hash::make('12345678'),
            'role'=>'member',
            'address'=>'Bandra, Mumbai',
            'efficiency'=>rand(10,100),
            'designation_id'=>\App\Designation::pluck('id')->random(),
            'team_id'=>2,
            'dob'=>date('Y-m-d',(strtotime("2001/04/13")))
        ])->tasks()->saveMany(factory(Task::class,rand(1,3))->make());
        \App\User::create([
            'name'=>'Jane Doe',
            'email'=>'jane@gmail.com',
            'password'=>Hash::make('12345678'),
            'role'=>'member',
            'address'=>'Worli, Mumbai',
            'efficiency'=>rand(10,100),
            'designation_id'=>\App\Designation::pluck('id')->random(),
            'team_id'=>1,
            'dob'=>date('Y-m-d',(strtotime("2001/04/13")))
        ])->tasks()->saveMany(factory(Task::class,rand(1,3))->make());
        
        
       
    }
}
