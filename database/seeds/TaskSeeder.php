<?php

use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Task::create([
            'name'=>'Create Skeleton UI',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'Analyze Data',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'Design database',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'Business Logic',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'DB Fetch to UI',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'Animations',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'JavaScript Live Preview',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'Clean the code',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'Photoshop Edits',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
        \App\Task::create([
            'name'=>'Test Modules',
            'status'=>'assigned',
            'user_id'=>rand(3,7),
            'team_id'=>rand(1,2),
            'assigned_on'=>\Carbon\Carbon::now()->format('Y-m-d'),
            'due_on'=>\Carbon\Carbon::tomorrow()->format('Y-m-d')
        ]);
    }
}
