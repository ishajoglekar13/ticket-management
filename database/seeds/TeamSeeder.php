<?php

use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Team::create([
            'name'=>'Blog Management',
            'leader_id'=>1
        ]);
        \App\Team::create([
            'name'=>'ERP Lite',
            'leader_id'=>2
        ]);
        \App\Team::create([
            'name'=>'Ticket Management',
            'leader_id'=>1
        ]);
    }
}
