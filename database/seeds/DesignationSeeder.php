<?php

use Illuminate\Database\Seeder;

class DesignationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \App\Designation::create([
            'name'=>'UI Designer'
        ]);
        \App\Designation::create([
            'name'=>'Data Analyist'
        ]);
        \App\Designation::create([
            'name'=>'UX Designer'
        ]);
        \App\Designation::create([
            'name'=>'Backend Developer'
        ]);
        \App\Designation::create([
            'name'=>'Software Tester'
        ]);
        \App\Designation::create([
            'name'=>'Full Stack Developer'
        ]);
        \App\Designation::create([
            'name'=>'DB Designer'
        ]);
        \App\Designation::create([
            'name'=>'DevOps Specialist'
        ]);
    }
}
