<?php

use App\Designation;
use App\Task;
use App\Team;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DesignationSeeder::class);
       
        factory(\App\Team::class,2)->create();
        
        factory(\App\User::class,10)->create()->each(function($user){
            $user->tasks()->saveMany(factory(Task::class,rand(1,3))->make());
        });
        $this->call(UserSeeder::class);
        
    }
}

