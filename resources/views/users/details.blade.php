@extends('dashboard.layout')
@section('title','Ticket Management')
@section('sub-title','Users')

@section('page-level-styles')

    <style>
         .displayNone{
    display:none;
 } 
    </style>
    @endSection

@section('main-content')
<div class="d-flex justify-content-end mb-3">
   
    </div>

        <div class="card">
            <div class="card-header"><img src="{{ $user->avatar_small }}" alt=""> {{$user->name}} </div>

            <div class="card-body">
                <div class="personal-details ">
                    <label for="Team">Team Name : {{$user->team->name}}</label> <br>
                    <label for="Team">Role : {{$user->role}}</label> <br>
                    <label for="Team">Email : {{$user->email}}</label> <br>
                    <label for="Team">DOB : {{$user->dob}}</label> <br>
                    <label for="Team">Address : {{$user->address}}</label><br><br>
                    
                   
                    
                </div>
                @if($user->role == "member")
                <div class="personal-details ">
                    <label for="Team">Total Tasks : {{$tasks->count()}}</label><br>
                    <label for="Team">Active Tasks : {{$tasks->where('status','assigned')->count()}}</label><br>
                    <label for="Team">Completed Tasks: {{$tasks->where('status','resolved')->count()}}</label><br>
                    <label for="Team">Failed Tasks : {{$tasks->where('status','unresolved')->count()}}</label><br><br>
                    <h6 class="text-primary" style="font-weight:600;">Tasks</h6>
                    
                </div>
                
                <table class="table table-bordered">
                    <thead>
                    
                        <th>Task Name</th>
                        <th>Level</th>
                        <th>Status</th>
                        <th>Assigned On</th>
                        <th>Due On</th>    
                        <th>Completed On</th>                
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>
                                   {{$task->name}}
                                </td>
                                
                                <td>
                                    @if($task->level==0)
                                        Easy
                                    @elseif($task->level==1)
                                        Intermediate
                                    @else
                                        Difficult
                                    @endif
                                </td>
                                <td>
                                    {{$task->status}}
                                </td>
                                
                                <td>
                                    {{$task->assigned_date}}
                                </td>

                                <td>
                                    {{$task->due_date}}
                                </td>

                                <td>
                                @if(($task->status=="assigned" || !$task->status=='unresolved') && !$task->completed_on )
                                    In Progress
                                @elseif($task->status=="unresolved") 
                                    <p class="text-danger">Failed!</p> 
                                    @else
                                    {{$task->completed_date}}
                                @endif 
                                </td>
                            </tr>
                           
                        @endforeach
                        
                    </tbody>
                </table>
                @endif
                {{$tasks->links()}}
                <div class="d-flex justify-content-around m-5 ">
                    <button class="btn btn-primary" id="adv-details">View Advanced Details &nbsp; <span id="innerdetails" class="displayNone">  <i class="fa fas fa-chevron-down"></i></span></button>
                </div> 

                <table class="table table-bordered displayNone" id="logsTable">
                    <thead>
                    
                        <th>Logged In</th>
                        <th>Logged Out</th>
                        <th>Tasks Completed</th>
                                   
                    </thead>
                    <tbody>
                        @foreach($logs as $log)
                        <tr>

                            <td>{{$log->logged_in}}</td>
                            <td>{{$log->logged_out}}</td>
                            <td>                                
                                @foreach($logstasks as $logstask)
                                
                                    @if((\Carbon\Carbon::parse($logstask->completed_on)->gt(\Carbon\Carbon::parse($log->logged_in))) && (\Carbon\Carbon::parse($logstask->completed_on)->lt(\Carbon\Carbon::parse($log->logged_out))))
                                        <i class="fa fa-star text-warning"></i>
                                    
                                    @endif
                                @endforeach
                            </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    
@endsection

@section('page-level-scripts')

<script>

var table = document.getElementById('logsTable');
var innerdetails = document.getElementById('innerdetails');

var btn = document.getElementById('adv-details');
btn.addEventListener('click',function(){
    setTimeout(function(){
        table.classList.toggle("displayNone");
        innerdetails.classList.toggle("displayNone");
    },500);
    
});

</script>
@endsection