@extends('dashboard.layout')
@section('title','Ticket Management')
@section('sub-title','Users')
@section('main-content')

    <div class="card">
        <div class="card-header">Users</div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Team</th>
                    <th>Role</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                            <img src="{{ $user->avatar_big }}" alt="">
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                            <td>
                            @if($user->team_id)
                                
                                {{$user->team->name}}
                            @else
                            <p class="text-danger">Currently not assigned</p>
                                @endif
                            </td>
                            <td>
                                @if($user->isLeader())
                                    Leader
                                @else
                                    Member
                                @endif
                            </td>
                            <td>
                                @if(!$user->team_id && $user->tasks->where('status','assigned')->count()==0)
                                    <form action="{{route('users.make-team-member',$user->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn btn-warning  btn-sm">Add To Team</button>
                                    </form>
                                @endif
                                @if($user->team_id == auth()->user()->team_id && $user->tasks->where('status','assigned')->count()==0  && $user->role != "leader")
                                    <form action="{{route('users.remove-team-member',$user->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="mb-3 btn btn-danger btn-sm">Remove from Team</button>
                                    </form>
                                @endif
                                <form action="{{route('users.details',$user->id)}}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-primary btn-sm">View Details</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $users->links()}}
    </div>
@endsection