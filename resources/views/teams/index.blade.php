@extends('dashboard.layout')
@section('title','Ticket Management')
@section('sub-title','Users')

@section('main-content')

<div class="d-flex justify-content-end mb-3">
    <a href="{{route('teams.create')}}" class="btn btn-primary">Add Member</a>
</div>
@if(auth()->user()->team_id)

    <div class="card">
        <div class="card-header">Team : {{auth()->user()->team->name}} </div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Completed Tasks</th>
                    <th>Failed Tasks</th>
                    <th>Active Tasks</th>
                    @if(auth()->user()->isLeader())
                    <th>Actions</th>
                    @endif
                </thead>
                <tbody>
                    @foreach($users as $user)
                        @if(!$user->isLeader())
                        <tr>
                            <td>
                                <img src="{{$user->avatar_big}}" alt="">
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                            
                            <td>
                                {{$user->tasks->where('status','resolved')->count()}}
                            </td>

                            <td>
                                {{$user->tasks->where('status','unresolved')->count()}}
                            </td>
                            <td>
                                {{$user->tasks->where('status','assigned')->count()}}
                            </td>
                            @if(auth()->user()->isLeader())
                            <td>

                                @if($user->role != "leader")
                                <form action="{{route('tasks.assign',$user->id)}}" method="POST">
                                    @csrf
                                   
                                    <button type="submit" class="btn btn-outline-primary btn-sm">Assign Task</button>
                                </form>
                              
                                @if($user->team_id == auth()->user()->team_id && $user->tasks->where('status','assigned')->count()==0  && $user->role != "leader")
                                    <form action="{{route('users.remove-team-member',$user->id)}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Remove from Team</button>
                                    </form>
                                    @endif
                                    @endif
                            </td>
                            @endif
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="card">
    
    <div class="card-header">Not a part of any team! </div>
    @endif
@endsection