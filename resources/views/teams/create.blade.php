@extends('dashboard.layout')
@section('title','Ticket Management')
@section('sub-title','Users')

@section('main-content')

    <div class="card">
        <div class="card-header">Users</div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>All Tasks</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <img src="{{Gravatar::src($user->email)}}" alt="">
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                          
                            <td>
                                {{$user->tasks->count()}}
                            </td>
                            <td>
                                @if(!$user->team_id && $user->tasks->where('status','assigned')->count()==0)
                                    <form action="{{route('users.make-team-member',$user->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn btn-outline-primary  btn-sm">Add to Team</button>
                                    </form>
                                @endif
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection