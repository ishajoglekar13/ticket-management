@extends('dashboard.layout')
@section('title','Ticket Management')
@section('sub-title','Users')

@section('main-content')

<div class="d-flex justify-content-end mb-3">
    <a href="{{route('tasks.create')}}" class="btn btn-primary">Add Task</a>
</div>

    <div class="card">
        <div class="card-header">My Tasks :  </div>

        <div class="card-body">
            <table class="table table-bordered table-responsive">
                <thead>
                   
                    <th>Name</th>
                    
                    <th>Level</th>
                    <th>Status</th>
                    <th>Assigned On</th>
                    <th>Due On</th>    
                    <th>Completed On</th>                
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                           
                            <td>
                                {{$task->name}}
                            </td>
                            
                            
                            <td>
                                @if($task->level==0)
                                    Easy
                                @elseif($task->level==1)
                                    Intermediate
                                @else
                                    Difficult
                                @endif
                            </td>
                            <td>
                                {{$task->status}}
                            </td>
                            
                            
                            <td class= "{{Carbon\Carbon::now()->gt(Carbon\Carbon::parse($task->due_on)) ? 'text-danger' : ''}}">
                                {{$task->assigned_date}}
                            </td>

                            <td class="{{Carbon\Carbon::now()->gt(Carbon\Carbon::parse($task->due_on)) ? 'text-danger' : ''}} ">
                                <div id="simple_timer<?=$task['id']?>" >
                                   
                                </div>
                            </td>

                            <td>
                             
                                @if(($task->status=="assigned" || !$task->status=='unresolved') && !$task->completed_on )
                                    In Progress
                                @elseif($task->status=="unresolved") 
                                    <p class="text-danger">Failed!</p> 
                                    @else
                                    {{$task->completed_date}}
                                @endif 
                            </td> 
                            
                            <td>
                             
                            @if($task->status=='assigned')
                                <a href="{{route('tasks.completed',$task->id)}}" class="btn btn-sm  {{Carbon\Carbon::now()->gt(Carbon\Carbon::parse($task->due_on)) ? 'btn-danger' : 'btn-primary'}}" >Complete</a>

                                <a href="{{route('tasks.giveup',$task->id)}}" class="btn btn-sm btn-warning" >Give Up</a>
                            @endif
                              
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
        {{$tasks->links()}}
    </div>
@endsection
@section('page-level-styles')
<link rel="stylesheet" href="{{asset('assets/timer/resources/default.css')}}">
@endsection
@section('page-level-scripts')
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{asset('assets/timer/build/jquery.syotimer.js')}}"></script>
<script src="{{asset('assets/timer/build/jquery.syotimer.min.js')}}"></script>

<script>
    
    <?php
    foreach($tasks as $task){
        if($task['status'] == "assigned"){ 
           ?>
    $('#simple_timer<?=$task['id']?>').syotimer({
        year: new Date("<?=$task['due_on']?>").getFullYear(),
        month: (new Date("<?=$task['due_on']?>").getMonth() + 1),
        day: new Date("<?=$task['due_on']?>").getDate(),
        hour:new Date("<?=$task['due_on']?>").getHours(),
        minute:new Date("<?=$task['due_on']?>").getMinutes()
    });
<?php } }  ?>
    
     
</script>



@endsection