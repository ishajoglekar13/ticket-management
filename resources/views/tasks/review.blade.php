@extends('dashboard.layout')
@section('title','Ticket Management')
@section('sub-title','Users')

@section('main-content')

<div class="d-flex justify-content-end mb-3">
    <a href="{{route('tasks.create')}}" class="btn btn-primary">Add Task</a>
</div>

    <div class="card">
        <div class="card-header">My Tasks :  </div>

        <div class="card-body">
            <table class="table table-bordered table-responsive">
                <thead>
                   
                    <th>Name</th>
                    <th>Level</th>
                    <th>Status</th>
                    <th>Assigned To</th>
                    <th>Assigned On</th>
                    <th>Due On</th>                  
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                           
                            <td>
                                {{$task->name}}
                            </td>
                            
                            
                            <td>
                                @if($task->level==0)
                                    Easy
                                @elseif($task->level==1)
                                    Intermediate
                                @else
                                    Difficult
                                @endif
                            </td>
                            <td>
                                {{$task->status}}
                            </td>
                            <td>
                                {{$task->user->name}}
                            </td>
                            
                            <td>
                                {{$task->assigned_date}}
                            </td>

                            <td>
                                {{$task->due_date}}
                            </td>
                            <td>
                             
                            @if($task->status=='completed')
                                <a href="{{route('tasks.resolved',$task->id)}}" class="btn btn-sm btn-outline-primary" >Resolve</a>
                                <a href="{{route('tasks.unresolved',$task->id)}}" class="btn btn-sm btn-outline-primary" >Reject</a>
                            @elseif( $task->status=='unresolved')
                            <a href="{{route('tasks.reassign',$task->id)}}" class="btn btn-sm btn-outline-danger" >Re-Assign</a>
                            @endif
                              
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
       
    </div>
@endsection
@section('page-level-scripts')


@endsection