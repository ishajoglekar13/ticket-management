@extends('dashboard.layout')
@section('title','Ticket Management')
@section('sub-title','Users')

@section('main-content')

<div class="d-flex justify-content-end mb-3">
    <a href="{{route('tasks.create')}}" class="btn btn-primary">Add Task</a>
</div>

    <div class="card">
        <div class="card-header">Manage Tasks :  </div>

        <div class="card-body">
            <table class="table table-bordered table-responsive">
                <thead>
                   
                    <th>Name</th>
                    <th>Assigned To</th>
                    <th>Level</th>
                    <th>Status</th>
                    <th>Assigned On</th>
                    <th>Due On</th>    
                    <th>Completed On</th>                
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                           
                            <td>
                                {{$task->name}}
                            </td>
                            <td>
                                {{$task->user->name}}
                            </td>
                            
                            <td>
                                @if($task->level==0)
                                    Easy
                                @elseif($task->level==1)
                                    Intermediate
                                @else
                                    Difficult
                                @endif
                            </td>
                            <td>
                                {{$task->status}}
                            </td>
                            
                            <td>
                                {{$task->assigned_date}}
                            </td>

                            <td>
                                {{$task->due_date}}
                            </td>

                            <td>
                            @if(($task->status=="assigned" || !$task->status=='unresolved') && !$task->completed_on )
                                    In Progress
                            @elseif($task->status=="unresolved") 
                                    <p class="text-danger">Failed!</p> 
                            @else
                                    {{$task->completed_date}}
                            @endif 
                            </td>
                            
                            <td>
                                @if($task->status=='assigned'||!$task->status=='unresolved')
                                <a href="{{route('tasks.edit',$task->id)}}" class="btn btn-sm btn-primary" >Edit</a>
                                @elseif($task->status=='completed')
                                <a href="{{route('tasks.review')}}" class="btn btn-outline-primary btn-sm">Review</a>
                              @endif
                              
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
@endsection