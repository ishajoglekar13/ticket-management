@extends('dashboard.layout')

@section('main-content')
    <div class="card">
        <div class="card-header">Add Task</div>
        <div class="card-body">
            <form action="{{ route('tasks.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Task Name</label>
                    <input type="text"
                           value="{{ old('name') }}"
                           class="form-control @error('name') is-invalid @enderror"
                           name="name" id="name">
                    @error('name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                
                @if(!$assigned)
                <div class="form-check mb-3">
                    <input class="form-check-input" type="checkbox" value="" id="bestUser">
                    <label class="form-check-label" for="bestUser">
                    Assign Task Automatically </label>
                </div>
                @endif

                
                <div class="form-group">
                    <label for="user_id">Assign to : </label>
                    <select name="user_id" id="user_id" class="form-control select2">
                    @if(!$assigned)
                        @foreach($users as $user)
                            
                            <option value="{{$user->id}}">{{$user->name}}</option>
                           
                        @endforeach
                    @else
                         
                            <option value="{{$user->id}}" selected>{{$user->name}}</option>
                      
                    @endif;

                  
                    </select>
                </div>


                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control select2">
                        <option value="assigned" selected>Assigned</option>
                        <option value="unresolved" disabled>Unresolved</option>
                        <option value="completed" disabled>Completed</option>
                        <option value="resolved" disabled>Resolved</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="level">Level</label>
                    <select name="level" id="level" class="form-control select2">
                        <option value="0">Easy</option>
                        <option value="1">Intermediate</option>
                        <option value="2">Difficult</option>
                        
                    </select>
                </div>

                <div class="form-group">
                    <label for="assigned_on">Assigned On</label>
                    <input type="date"
                           value="{{ old('assigned_on')}}"
                           class="form-control"
                           name="assigned_on" id="assigned_on" >
                </div>


                <div class="form-group">
                    <label for="due_on">Due On</label>
                    <input type="date"
                           value="{{ old('due_on') }}"
                           class="form-control"
                           name="due_on" id="due_on">
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Assign Task</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('page-level-scripts')

    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        var level = document.getElementById('level');
        
        flatpickr("#assigned_on", {
            enableTime: true,    
            minDate:new Date(),
            defaultDate:new Date()        
        });

        
        var assigned_on =document.getElementById('assigned_on');
        var a = new Date(assigned_on.value); 

        flatpickr("#due_on", {
            enableTime: true,
            minDate:new Date(assigned_on.value).fp_incr((parseInt(level.value)+1)),
            defaultDate:new Date(assigned_on.value).setDate(a.getDate() + (parseInt(level.value)+1))      
            
        });

        function setValue(){
            flatpickr("#due_on", {
            enableTime: true,
            defaultDate:new Date(assigned_on.value).setDate(a.getDate()+ (parseInt(level.value)+1)),
            minDate: new Date(assigned_on.value).setDate(a.getDate()+ (parseInt(level.value)+1))
            });
            
        }
        
        assigned_on.addEventListener('change',function(){
            setValue();
        });
        level.addEventListener('change',function(){
           setValue();
        }); 


        var assign_to = document.getElementById('bestUser');
        var assign_list = document.getElementById('user_id');
        function assignTask(){
            if(assign_to.checked == true){
                
                <?php
                    use App\Task;
                    $task = new Task();
                    $user = $task->getEfficientUser();
                ?>
                assign_list.innerHTML = `<option value="<?=$user->id?>" selected><?=$user->name?></option>`
            }
            else{
                assign_list.innerHTML = "";
                <?php
                    foreach($users as $user)
                    {?>
                        assign_list.innerHTML += `<option value="<?=$user->id?>" selected><?=$user->name?></option>`
                    <?php
                    }
                    ?>

            }
        }

        assign_to.addEventListener('change',function(){
            assignTask();
        });
    </script>

     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2_locale_ar.min.js"></script>
@endsection
@section('page-level-styles')

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2.min.css">
@endsection