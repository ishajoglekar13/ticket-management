@extends('dashboard.layout')

@section('main-content')
    <div class="card">
        <div class="card-header">Edit Task</div>
        <div class="card-body">
            <form action="{{ route('tasks.update',$task->id) }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Task Name</label>
                    <input type="text"
                           value="{{ old('name',$task->name) }}"
                           class="form-control @error('name') is-invalid @enderror"
                           name="name" id="name">
                    @error('name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>


                
                <div class="form-group">
                    <label for="user_id">Assign to : </label>
                    <select name="user_id" id="user_id" class="form-control select2">
                        <option value="{{$user->id}}" {{$user->id == $task->user_id ? 'selected' : ''}}>{{$user->name}}</option>              
                    </select>
                </div>


                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control select2">
                        <option value="assigned" selected>Assigned</option>
                       
                    </select>
                </div>

                <div class="form-group">
                    <label for="level">Level</label>
                    <select name="level" id="level" class="form-control select2">
                        <option  {{ $task->level == 0 ? 'selected' : '' }} value="0">Easy</option>
                        <option {{ $task->level == 1 ? 'selected' : '' }} value="1">Intermediate</option>
                        <option {{ $task->level == 2 ? 'selected' : '' }} value="2">Difficult</option>
                    
                    </select>
                </div>

                <div class="form-group">
                    <label for="assigned_on">Assigned On</label>
                    <input type="text"
                           value="{{ old('assigned_on',$task->assigned_on)}}"
                           class="form-control"
                           name="assigned_on" id="assigned_on" disabled >
                </div>


                <div class="form-group">
                    <label for="due_on">Due On</label>
                    <input type="text"
                           value="{{ old('due_on',$task->due_on) }}"
                           class="form-control"
                           name="due_on" id="due_on">
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Edit Task</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('page-level-scripts')

    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        var level = document.getElementById('level');
        

        var assigned_on =document.getElementById('assigned_on');
        var a = new Date(assigned_on.value); 

        flatpickr("#due_on", {
            enableTime: true,
            minDate:new Date(assigned_on.value).fp_incr((parseInt(level.value)+1)),
            defaultDate:new Date(assigned_on.value).setDate(a.getDate() + (parseInt(level.value)+1))      
            
        });

        function setValue(){
            flatpickr("#due_on", {
            enableTime: true,
            defaultDate:new Date(assigned_on.value).setDate(a.getDate()+ (parseInt(level.value)+1)),
            minDate: new Date(assigned_on.value).setDate(a.getDate()+ (parseInt(level.value)+1))
            });
            
        }
        
      
        level.addEventListener('change',function(){
           setValue();
        }); 



    </script>

     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2_locale_ar.min.js"></script>
@endsection
@section('page-level-styles')

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2.min.css">
@endsection