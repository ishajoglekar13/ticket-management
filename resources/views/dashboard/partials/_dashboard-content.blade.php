 <!-- Content Row -->
 <div class="row">

<!-- Earnings (Monthly) Card Example -->
@if(auth()->user()->role == 'member')
<div class="col-xl-3 col-md-6 mb-4">
  <div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">All Tasks</div>
          <div class="h5 mb-0 font-weight-bold text-gray-800">{{auth()->user()->tasks->count()}}</div>
        </div>
        <div class="col-auto">
          <i class="fas fa-calendar fa-2x text-gray-300"></i>
        </div>
      </div> 
    </div>
  </div>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
  <div class="card border-left-success shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Completed Tasks</div>
          <div class="h5 mb-0 font-weight-bold text-gray-800">{{auth()->user()->tasks->where('status','resolved')->count()}}</div>
        </div>
        <div class="col-auto">
          <i class="fa fa-check-circle fa-2x text-gray-300"></i>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
  <div class="card border-left-info shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Active Tasks</div>
          <div class="row no-gutters align-items-center">
            <div class="col-auto">
              <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{auth()->user()->tasks->where('status','assigned')->count()}}</div>
            </div>
            
          </div>
        </div>
        <div class="col-auto">
          <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Pending Requests Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
  <div class="card border-left-warning shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Failed Tasks</div>
          <div class="h5 mb-0 font-weight-bold text-gray-800">{{auth()->user()->tasks->where('status','unresolved')->count()}}</div>
        </div>
        <div class="col-auto">
          <i class="fas fa-exclamation-triangle  fa-2x text-gray-300"></i>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Content Row -->
@endif
<div class="row">






<!-- Area Chart -->
<div class="col-xl-4 col-lg-7">

<div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary text-center">Employee of the Month </h6>
     
    </div>
    <!-- Card Body -->
    <div class="card-body">
      <div class="content text-center">
       <div class="icon wow rotateIn"><i class="fa fa-star fa-4x text-warning text-center mb-4"></i></div>

        <h3 class="text-center font-weight-bold text-warning mb-3">{{$users[0]->name}}</h3>
        <h6>Team : {{$users[0]->team->name}}</h6>
      </div>
    </div>
    
  </div>


 


  
</div>

<div class="col-xl-4">

<div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary text-center">Leader of the Month </h6>
     
    </div>
    <!-- Card Body -->
    <div class="card-body">
      <div class="content text-center">
       <div class="icon wow bounceIn"><i class="fa fa-user-tie fa-4x text-center mb-4" style="color:darksalmon;"></i></div>

        <h3 class="text-center font-weight-bold  mb-3" style="color: darksalmon;">{{$leader->name}}</h3>
        <h6>Team : {{$leader->team->name}}</h6>
      </div>
    </div>
    
  </div>
</div>


<div class="col-xl-4 col-lg-5">
  <div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Awards</h6>
    </div>
    <!-- Card Body -->
    <div class="card-body">
      <div class="row">
      
      @foreach($users as $user)
        
        <div class="col-md-8 mb-3">
        
          <p class="{{$users[0] === $user ? 'text-warning font-weight-bold' : ''}} ">{{$user->name}}</p>
        </div>
        
        <div class="col-md-4 mb-3">
        @if(($users[0] == $user) || ($users[1] == $user) || ($users[2] == $user))
        @if($users[0] === $user)
          <div class="col-md-12">
         
          <i class="fa fa-trophy fa-2x fas text-warning wow fadeIn"></i>
         
          </div>
          @endif

          @if($users[1] === $user)
          <div class="col-md-12">
         
          <i class="fa fa-trophy fa-2x fas wow fadeIn" style="color:darkgrey"></i>
         
          </div>
          @endif

          @if($users[2] === $user)
          <div class="col-md-12">
         
          <i class="fa fa-trophy fa-2x fas wow fadeIn"style="color:#b08d57;"></i>
         
          </div>
         @endif
         @else

         
          <div class="col-md-12">
         
        
          </div>
          @endif
        </div>
        
        @endforeach
      </div>
      
    </div>
    
  </div>

</div>

<!-- Pie Chart -->

</div>

<!-- Content Row -->
<div class="row">

<!-- Content Column -->
@if(!auth()->user()->isLeader())
<div class="col-lg-12 mb-4">

  <!-- Project Card Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Task Pole</h6>
    </div>
    <div class="card-body">
          <div class="row mb-3">
            <div class="col-md-4 font-weight-bold text-primary">Task Name</div>
            <div class="col-md-2 font-weight-bold text-primary">Level</div>
            <div class="col-md-4 font-weight-bold text-primary">Due On</div>
    </div>
        @foreach($polltasks as $polltask)
          <div class="row mb-2">
            <div class="col-md-4">{{$polltask->name}}</div>
            @if($polltask->level == 0)
              <div class="col-md-2">Easy</div>
            @elseif($polltask->level == 1)
              <div class="col-md-2">Intermediate</div>
            @elseif($polltask->level == 2)
              <div class="col-md-2">Difficult</div>
            @endif
            <div class="col-md-4">{{$polltask->due_on}}</div>
            <a href="{{route('tasks.claim',$polltask->id)}}" class="btn btn-sm p-0 btn-primary text-sm col-md-2">Claim</a>
          </div>
        @endforeach
    </div>
  </div>

</div>
@endif

<div class="col-lg-6 mb-4">

  <!-- Illustrations -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Illustrations</h6>
    </div>
    <div class="card-body">
      <div class="text-center">
        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="{{ asset('assets/img/undraw_posting_photo.svg')}}" alt="">
      </div>
      <p>Add some quality, svg illustrations to your project courtesy of <a target="_blank" rel="nofollow" href="https://undraw.co/">unDraw</a>, a constantly updated collection of beautiful svg images that you can use completely free and without attribution!</p>
      <a target="_blank" rel="nofollow" href="https://undraw.co/">Browse Illustrations on unDraw &rarr;</a>
    </div>
  </div>
</div>

  <!-- Approach -->
  <div class="col-lg-6 mb-4">
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Development Approach</h6>
    </div>
    <div class="card-body">
      <p>SB Admin 2 makes extensive use of Bootstrap 4 utility classes in order to reduce CSS bloat and poor page performance. Custom CSS classes are used to create custom components and custom utility classes.</p>
      <p class="mb-0">Before working with this theme, you should become familiar with the Bootstrap framework, especially the utility classes.</p>
    </div>
  </div>

</div>
</div>