@extends('dashboard.layout')

@section('title','Ticket Management')
@section('sub-title','Dashboard')
@section('page-level-styles')

<link rel="stylesheet" href="{{asset('assets/vendor/animate/animate.css')}}">

@endsection
@section('main-content')
    @include('dashboard.partials._dashboard-content')
@endsection

@section('page-level-scripts')
<!-- Page level plugins -->
<script src="{{asset('assets/vendor/chart.js/Chart.min.js')}}"></script>


<!-- Page level custom scripts -->
<script src="{{asset('assets/js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('assets/js/demo/chart-pie-demo.js')}}"></script>
<script src="{{asset('assets/vendor/wow/wow.js')}}"></script>
<script src="{{asset('assets/vendor/wow/main.js')}}"></script>
<script>
   
        <?php
            use App\Log;
            $log = Log::all()->where('session_id',session()->getId());
            if(empty($log[0])){
                $log = new Log();
                $log->createNewEntry();
                
            }

        ?>
    
    
</script>


@endsection