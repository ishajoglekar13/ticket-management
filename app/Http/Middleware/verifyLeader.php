<?php

namespace App\Http\Middleware;

use Closure;

class verifyLeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()->isLeader()){
            return redirect(abort(401));
        }
        return $next($request);
    }
}
