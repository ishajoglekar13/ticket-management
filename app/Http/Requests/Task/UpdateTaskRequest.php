<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'name'=>'required|max:255',
            'user_id'=>'required|exists:users,id',
            'assigned_on'=>'required',
            'due_on'=>'required',
            'status'=>'required',
            'level'=>'required',
            //
        ];
    }
}
