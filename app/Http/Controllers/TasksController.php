<?php

namespace App\Http\Controllers;

use App\Http\Requests\Task\CreateTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Notifications\AssignedTask;
use App\Notifications\CompletedTask;
use App\Notifications\GaveUpTask;
use App\Notifications\GaveUpTaskMember;
use App\Notifications\ResolvedTask;
use App\Notifications\UnresolvedTask;
use App\Task;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\TaskHelper;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::whereIn('user_id', function($query){
            $query->select('id')
            ->from(with(new User)->getTable())
            ->whereIn('id',User::all()->where('team_id',auth()->user()->team_id)->pluck('id')->toArray());
        })->orderBy('updated_at','DESC')->get();
        $sidebarSubSection = "manage-tasks";
        $sidebarSection = "tasks";
        return view('tasks.index',compact([
            'tasks',
            'sidebarSection',
            'sidebarSubSection'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $assigned=0;
        $users = User::all()->where('team_id',auth()->user()->team_id)->where('role','member');
        $sidebarSubSection = "add-tasks";
        $sidebarSection = "tasks";
        return view('tasks.create',compact([
            'users',
            'assigned',
            'sidebarSection',
            'sidebarSubSection'
        ]));
    }

    public function reassign(Task $task)
    {
        
        $assigned=2;
        $users = User::all()->where('team_id',auth()->user()->team_id);
        $restrictedUser=User::all()->where('id',$task->user_id)->pluck('id')[0];
        
        $sidebarSubSection = "manage-tasks";
        $sidebarSection = "tasks";
        
        $user = $task->user;
        $user->notify(new UnresolvedTask($task)); 
        return view('tasks.reassign',compact([
            'users',
            'task',
            'assigned',
            'restrictedUser',
            'sidebarSection',
            'sidebarSubSection'
        ]));
    }

   

    public function store(CreateTaskRequest $request)
    {
       
        $task = Task::create([
           'name'=>$request->name,
           'level'=>$request->level,
           'status'=>$request->status,
            'assigned_on'=>$request->assigned_on,
            'due_on'=>$request->due_on,
            'user_id'=>$request->user_id
        ]);
        if($request->user_id != NULL){
            $user = User::all()->where('id',$request->user_id)->first();
            $user->notify(new AssignedTask($task));
            session()->flash('success','Task Assigned Successfully');
            return redirect(route('tasks.index'));
        }
        
        return redirect(route('dashboard.index'));

    }    
   
    public function assign(User $user)
    {     
        
        $assigned=1;
        $sidebarSection = "tasks";
        $sidebarSubSection = "add-tasks";
         return view('tasks.create',compact([
             'user',
             'assigned',
             'sidebarSection',
            'sidebarSubSection'
         ]));
         
    }
    public function poll()
    {     
        $sidebarSection = "tasks";
        $sidebarSubSection = "poll-tasks";
         return view('tasks.poll',compact([
             'sidebarSection',
            'sidebarSubSection'
         ]));
         
    }
    public function review()
    {     
        $tasks = Task::whereIn('user_id', function($query){
            $query->select('id')
            ->from(with(new User)->getTable())
            ->whereIn('id',User::all()->where('team_id',auth()->user()->team_id)->pluck('id')->toArray());
        })->orderBy('updated_at','DESC')->get();
        $sidebarSection = "tasks";
        $sidebarSubSection = "review-tasks";
         return view('tasks.review',compact([
             'tasks',
             'sidebarSection',
            'sidebarSubSection'
         ]));
         
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $user = User::all()->where('id',$task->user_id)->first();
        // dd($users);
        // $restrictedUser=User::where('id',$task->user_id);
        $sidebarSection = "tasks";
        $sidebarSubSection = "manage-tasks";

        
        return view('tasks.edit',compact([
            'task',
            'user',
            'sidebarSection',
            'sidebarSubSection'
        ]));
    }

    public function mytasks()
    {
        $tasks = Task::latest('updated_at')->where('user_id',auth()->user()->id)->paginate(10);
        $sidebarSection = "tasks";
        $sidebarSubSection = "my-tasks";
        return view('tasks.mytasks',compact([
            'tasks',
            'sidebarSection',
            'sidebarSubSection'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        
        $data = $request->only(['name','user_id','level','status','assigned_on','due_on']);
       
        $task->update($data);
        session()->flash('success','Task Updated Successfully!');
        return redirect(route('tasks.index'));
    }

    public function claim(Task $task)
    {
        $task->update(['user_id'=>auth()->id(),'status'=>'assigned']);
        $task->save();
        session()->flash('success','Task claimed Successfully!');
        return redirect(route('dashboard.index'));
    }
    
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //

    }

    public function completed(Task $task)
    {
       
        $task->status = "completed";
        $task->completed_on = now();
        $task->save();
        
        $user = User::all()->where('team_id',auth()->user()->team_id)->where('role','leader')->first();
        
        $user->notify(new CompletedTask($task));
        return redirect(route('tasks.mytasks'));

    }
   
    public function resolved(Task $task)
    {
        
        $task->updateUserEfficiency($task->user,TaskHelper::getPoints($task));
        $task->updateLeaderEfficiency(auth()->user(),$task);
        $task->status = "resolved";
        $task->save();
       
        $user = $task->user;
        $user->notify(new ResolvedTask($task));
        return redirect(route('tasks.review'));

        
    }

    public function unresolved(Task $task)
    {
        $task->status = "unresolved";
        $task->save();
       
        $user = $task->user;
        $user->notify(new UnresolvedTask($task));
        return redirect(route('tasks.review'));

        
    }
    public function giveup(Task $task)
    {
        $task->giveupEfficiency($task->user,$task);
        $task->status = "unresolved";
        $task->save();

        $user = User::all()->where('team_id',auth()->user()->team_id)->where('role','leader')->first();
        $member = $task->user;
        $user->notify(new GaveUpTask($task));
        $member->notify(new GaveUpTaskMember($task));
        
        return redirect(route('tasks.mytasks'));
    }
}
