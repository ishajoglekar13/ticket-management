<?php

namespace App\Http\Controllers;

use App\User;
use App\Task;
use Illuminate\Http\Request;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {  
        $sidebarSection = "dashboard";
        $sidebarSubSection = "";
        $users = User::where('role','member')->orderBy('efficiency','DESC')->take(3)->get();
        $leader = User::where('role','leader')->orderBy('efficiency','DESC')->first();
        $polltasks = Task::where('status','poll')->orderBy('due_on','ASC')->get();
        return view('dashboard.index',compact([
            'sidebarSection',
            'sidebarSubSection',
            'users',
            'polltasks',
            'leader'
        ]));
    }
}
