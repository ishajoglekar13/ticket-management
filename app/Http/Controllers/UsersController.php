<?php

namespace App\Http\Controllers;

use App\Log;
use App\Notifications\AddedToTeam;
use App\Notifications\RemovedFromTeam;
use App\User;
use App\Task;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $users = User::latest('updated_at')->paginate(10);
        $sidebarSection = 'users';
        $sidebarSubSection = '';
        return view('users.index',compact([
           'users',
           'sidebarSection',
           'sidebarSubSection'
        ]));
    }

    public function makeTeamMember(User $user) {
        
        $user->update(['team_id'=>auth()->user()->team_id,'role'=>'member']);
        session()->flash('success', $user->name . ' has been assigned to your team now!');
        $user->notify(new AddedToTeam($user));
        
        return redirect(route('teams.index'))->with('flash_message', 'This is my flash message!');
        //return redirect(route('teams.index'));
    }

    public function removeTeamMember(User $user) {
        
        $user->update(['team_id'=>NULL,'role'=>'NULL']);
        session()->flash('success', $user->name . ' has been removed from your team now!');
        $user->notify(new RemovedFromTeam($user));
        return redirect(route('teams.index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function details(User $user){
        $sidebarSection = 'users';
        $sidebarSubSection = '';
        $tasks = Task::where('user_id',$user['id'])->latest('updated_at')->paginate(10);
        $logs = Log::all()->where('user_id',$user['id'])->where('logged_out','!=',NULL);
        $logstasks = Task::all()->where('status','completed')->where('user_id',$user['id']);
        
        return view('users.details',compact([
           'user',
           'tasks',
           'sidebarSection',
           'sidebarSubSection',
           'logs',
           'logstasks'
        ]));
    }
}
