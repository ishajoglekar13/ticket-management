<?php

namespace App\Console\Commands;

use App\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MinutelyCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minute:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check all the tasks every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd(DB::table('tasks')->update());
        
        $tasks = Task::all();
        dd($tasks);
        foreach($tasks as $task){
            
            if(Carbon::now()->gt(Carbon::parse($task->due_on)))
            {
                $task->status = "unresolved";
                $task->save();
            }
        }

       
    }
}
