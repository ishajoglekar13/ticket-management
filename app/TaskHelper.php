<?php

namespace App;

use Carbon\Carbon;
use App\Task;
use Illuminate\Database\Eloquent\Model;

class TaskHelper extends Task
{
    public static function getPoints(Task $task)
    {  
        $diff = Carbon::parse($task->due_on)->diffInHours(Carbon::parse($task->completed_on));
        $completed_within =Carbon::parse($task->assigned_on)->diffInHours(Carbon::parse($task->completed_on));
        if($task->level==0)
        {
           
            if(Carbon::parse($task->due_on)->gt(Carbon::parse($task->completed_on))){
                if($completed_within < 8)
                {
                   return 10;
                }elseif($completed_within < 12){
                    return 9;
                }elseif($completed_within < 16){
                    return 8;
                }elseif($completed_within < 20){
                    return 7;
                }
                else{
                    return 6;
                }
            }
            elseif(Carbon::parse($task->due_on)->lt(Carbon::parse($task->completed_on)))
            {
                if($completed_within < 8)
                {
                   return 5;
                }elseif($completed_within < 12){
                    return 4;
                }elseif($completed_within < 16){
                    return 3;
                }elseif($completed_within < 20){
                    return 2;
                }
                else{
                    return 1;
                }
                              
            }
        }elseif($task->level==1)
        {
            
            if(Carbon::parse($task->due_on)->gt(Carbon::parse($task->completed_on))){
                if($completed_within < 12)
                {
                   return 16;
                }elseif($completed_within < 18){
                    return 15;
                }elseif($completed_within < 24){
                    return 14;
                }elseif($completed_within < 30){
                    return 13;
                }elseif($completed_within < 36){
                    return 12;
                }elseif($completed_within < 48){
                    return 11;
                }else{
                    return 10;
                }
            }
            elseif(Carbon::parse($task->due_on)->lt(Carbon::parse($task->completed_on)))
            {
                if($completed_within < 12)
                {
                   return 9;
                }elseif($completed_within < 18){
                    return 8;
                }elseif($completed_within < 24){
                    return 7;
                }elseif($completed_within < 30){
                    return 6;
                }elseif($completed_within < 36){
                    return 5;
                }elseif($completed_within < 48){
                    return 4;
                }else{
                    return 3;
                }
                              
            }
        }elseif($task->level==2)
        {
            
            if(Carbon::parse($task->due_on)->gt(Carbon::parse($task->completed_on))){
                if($completed_within < 18)
                {
                   return 20;
                }elseif($completed_within < 26){
                    return 19;
                }elseif($completed_within < 32){
                    return 18;
                }elseif($completed_within < 40){
                    return 17;
                }elseif($completed_within < 48){
                    return 16;
                }elseif($completed_within < 56){
                    return 15;
                }elseif($completed_within < 62){
                    return 14;
                }else{
                    return 13;
                }
            }
            elseif(Carbon::parse($task->due_on)->lt(Carbon::parse($task->completed_on)))
            {
                if($completed_within < 18)
                {
                   return 12;
                }elseif($completed_within < 26){
                    return 11;
                }elseif($completed_within < 32){
                    return 10;
                }elseif($completed_within < 40){
                    return 9;
                }elseif($completed_within < 48){
                    return 8;
                }elseif($completed_within < 56){
                    return 7;
                }elseif($completed_within < 62){
                    return 6;
                }else{
                    return 5;
                }
                              
            }
        }
            
    }
    

}