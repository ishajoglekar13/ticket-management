<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Kernel as ConsoleKernal;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name','status','assigned_on','due_on','completed_on','level','user_id'];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
    public function team()
    {
        return $this->belongsTo(\App\Team::class);
    }
    public function getAssignedDateAttribute(){
        return (Carbon::parse($this->assigned_on))->diffForHumans();
    } 
    public function getDueDateAttribute(){
        return (Carbon::parse($this->due_on))->diffForHumans();
    } 
    public function getCompletedDateAttribute(){
        return (Carbon::parse($this->completed_on))->diffForHumans();
    } 
      
    
    
    public function updateUserEfficiency(User $user , int $points){
        $user->efficiency = $user->efficiency+$points;
        $user->save();
        
    }

    public function updateLeaderEfficiency(User $leader,Task $task){
        
        if($task->level == 0 )
            $points = 1;
        else if($task->level==1)
            $points = 2;
        else if($task->level ==2)
            $points =3;

        $leader->efficiency = $leader->efficiency+$points;
        $leader->save();
    }

    public function giveupEfficiency(User $user,Task $task)
    {
        if($task->level==0)
            $points = 6;
        else if($task->level==1)
            $points = 10;
        else if($task->level == 2)
            $points = 13;

        $user->efficiency = $user->efficiency - $points;
        $user->save();
    }

    public function getEfficientUser(){
        
        $users = User::orderBy("efficiency",'DESC')->where('team_id',auth()->user()->team_id)->where('role','member')->get();
        $newEfficiencyArray = array();
        foreach ($users as $user) {
            $flag = true;
            $totalHours = 0;
            $count = 0;
            $task = Task::all()->where('user_id',$user['id'])->where('status','assigned')->pluck('level');
            
            if($task->count() == 0)
            {
                $eligible = Carbon::now()->diffInDays(Carbon::parse($user->created_at));
                if($eligible >= 30){
                    return $user;   
                }
                $flag = false;            
            }
            
            elseif($flag){
                for($i = 0;$i < $task->count() ; $i++){
                    $level = $task[$i];
                    if($level == 0){$totalHours += 1;}
                    elseif($level == 1){$totalHours += 2;}
                    elseif($level == 2){$totalHours += 3;}
                
                    $newEfficiency = $user['efficiency']/$totalHours;
                    array_push($newEfficiencyArray,$newEfficiency);
                }
            }
        } 
        $userFinal = array_search(max($newEfficiencyArray),$newEfficiencyArray);
        return $users[$userFinal];   
        
    }
    

}
