<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address','dob','team_id','role','designation_id','efficiency'
    ];

    /**designation
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function isLeader()
    {
        return $this->role === 'leader';
    }
    
    /**
     * Relationship methods
     */
    
     public function team()
     {
            return $this->belongsTo(\App\Team::class);
         
     }
     public function tasks()
     {
            return $this->hasMany(\App\Task::class);
         
     }

     public function logs()
     {
            return $this->hasMany(\App\Log::class);
     
     }

     public function designation()
     {
            return $this->belongsTo(\App\Designation::class);
         
     }

     public function getAvatarSmallAttribute(){
        $size = 40;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}";
    }
    public function getAvatarBigAttribute(){
        $size = 70;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&size={$size}";
    }
}
