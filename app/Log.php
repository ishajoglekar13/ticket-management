<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Log extends Model
{
    protected $fillable = ['user_id','logged_in','logged_out','session_id'];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
    public function createNewEntry(){
        $log = new Log();
        $log->user_id = auth()->id();
        $log->logged_in = \Carbon\Carbon::now();
        $log->session_id = session()->getId();
        $log->save(); 
    }
    public function updateEntry(){
        $log = Log::all()->where('session_id',session()->getId())->first();
        $log->logged_out = \Carbon\Carbon::now();
        $log->save(); 
    }
}
